﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using _20210804.Data;
using _20210804.Models;

namespace _20210804.Pages.Movies
{
    public class IndexModel : PageModel
    {
        private readonly _20210804.Data._20210804Context _context;

        public IndexModel(_20210804.Data._20210804Context context)
        {
            _context = context;
        }

        public IList<Movie> Movie { get;set; }

        public async Task OnGetAsync()
        {
            Movie = await _context.Movie.ToListAsync();
        }
    }
}
