﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using _20210804.Models;

namespace _20210804.Data
{
    public class _20210804Context : DbContext
    {
        public _20210804Context (DbContextOptions<_20210804Context> options)
            : base(options)
        {
        }

        public DbSet<_20210804.Models.Movie> Movie { get; set; }
    }
}
